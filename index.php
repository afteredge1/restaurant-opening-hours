<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TDS</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="assets/css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script>
    <script type="text/javascript" src="assets/js/datetimepicker.js"></script>

    <style>
        .container-fluid {
            max-width: 970px;
            margin-top: 30px;
        }
    </style>
</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm"></div>
        </div>
        <p>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Select date time to find Hotels list:</label>
                <div>
                    <div id="picker"></div>
                    <input type="hidden" id="datetime" value="<?php echo date("Y-m-d H:i"); ?>" />
                </div>
                <br>
                <input type=" button" value="Find" class="btn btn-primary" onclick="fetchHotelsList();">
            </div>
        </div>
        </p>

        <p>
        <div class="row">
            <div class="col-sm-8">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Hotels List</th>
                        </tr>
                    </thead>
                    <tbody id="hotelList">
                    </tbody>
                </table>

                <div id="message" style="color:#6e5019;">
                    Please select date to proceed
                </div>
            </div>
        </div>
        </p>

    </div>

</body>

<script type="text/javascript">
    $(document).ready(function() {
        $('#picker').dateTimePicker();
    });

    function fetchHotelsList() {
        $.ajax({
            url: "fetchHotelList.php",
            type: 'GET',
            data: {
                "date": $("#datetime").val()
            },
            dataType: 'json',
            success: function(res) {

                // clear table of the old data other than the header row
                $("#hotelList").empty();

                if (res.length) {
                    $("#message").hide();
                    for (let i = 0; i < res.length; i++) {
                        $('#hotelList').append('<tr><td>' + res[i] + '</td></tr>');
                    }
                } else {
                    $("#message").empty().append('Please try another date, no record found!');
                    $("#message").show();
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.responseText)
            }
        });
    }
</script>

</html>
