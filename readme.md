# Requirements

Given the attached CSV data file - uploads/rest_open_hours.csv

1. Write a function that parses the data into a table
2. Write a function that receives a date object and uses a sql query to return a list of restaurant names which are open on that date and time.

# Assumptions

1. If a day of the week is not listed, the restaurant is closed on that day
2. All times are local — don’t worry about timezone-awareness
3. The CSV file will be well-formed

# User guide

List of restaurant names which are open on that date and time.

Set your database credentials in

`config.php`

For application database installation follow these steps

`http://localhost/restaurant-opening-hours/install.html`

This page has 2 steps:

1- Install database.

2- Seed data.

To access application use following URL

`http://localhost/restaurant-opening-hours/`

CSV file is placed in uploads folder.

Data parsing is done in ImportData class.
