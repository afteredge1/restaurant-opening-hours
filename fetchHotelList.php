<?php

require_once __DIR__ . '/vendor/autoload.php';

use TDS\HotelClass;

$hotels = new HotelClass();

$dateTime = '';
$day = '';
$mins = '';

if (isset($_REQUEST['date'])) {
    $dateTime = $_REQUEST['date'];

    list($date, $time) = explode(' ', $dateTime);
    $day = date('D', strtotime($date));
    list($hour, $min) = explode(':', $time);
    // handle case for time upto 2:59 AM
    if ($hour < 3) {
        $hour += 24;
    }
    $mins = $hour * 60 + $min;
}

echo $hotels->index($day, $mins);
