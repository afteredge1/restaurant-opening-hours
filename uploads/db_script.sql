/* drop database for fresh installation */

DROP DATABASE IF EXISTS hotel_open_hours;

/* create database */
CREATE DATABASE IF NOT EXISTS hotel_open_hours;

/* use database */
USE hotel_open_hours;

/* create table hotel list */
CREATE TABLE IF NOT EXISTS hotel_list (
id int UNSIGNED AUTO_INCREMENT PRIMARY KEY,
hotel_name VARCHAR(100)
);

/* create table opening hours */
CREATE TABLE IF NOT EXISTS operational_hours (
id int UNSIGNED AUTO_INCREMENT PRIMARY KEY,
hotel_id int,
day_name VARCHAR(3),
open_time int,
close_time int
);
