<?php

namespace TDS;

use TDS\contracts\HotelClassContract;
use TDS\DBConClass;

class HotelClass implements HotelClassContract
{
    public function index($day = 'Sun', $mins = 720)
    {
        $response = [];

        $result = self::fetchRecords($day, $mins);

        // process resultset
        if (mysqli_num_rows($result) > 0) {
            foreach ($result as $key => $value) {
                $response[$key] = $value['hotel_name'];
            }
            return json_encode($response);
        }

        return json_encode($response);
    }

    public function fetchRecords($day, $mins)
    {
        $dbCon = new DBConClass();
        $connection = $dbCon->connect();

        $sqlHotelList = "SELECT hotel_list.hotel_name FROM `operational_hours` 
            LEFT JOIN hotel_list ON hotel_list.id = operational_hours.hotel_id
            WHERE day_name = '" . $day . "' AND '" . $mins . "' BETWEEN open_time AND close_time ORDER BY `hotel_id` ASC;";
        $result = mysqli_query($connection, $sqlHotelList);

        // close database connection
        $dbCon->disconnect($connection);

        return $result;
    }
}
