<?php

namespace TDS;

use TDS\DBConClass;

class ImportData
{
    public function __construct()
    {
    }

    public function seedRecords()
    {
        // import file name & path
        $fileName = 'uploads/rest_open_hours.csv';

        $dbCon = new DBConClass();
        $connection = $dbCon->connect();
        $file = fopen($fileName, "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            $hotelTimingList = self::parseCSV($getData[1]);

            $sqlHotel = "INSERT INTO hotel_list (hotel_name) 
                        values ('" . addslashes($getData[0]) . "')";
            mysqli_query($connection, $sqlHotel);
            $hotelId = mysqli_insert_id($connection);

            foreach ($hotelTimingList as $value) {
                list($day, $openTime, $closeTime) = explode('-', $value);

                $mins = self::minutes($openTime, $closeTime);
                list($openMins, $closeMins) = explode('-', $mins);
                $sqlTiming = "INSERT INTO operational_hours (hotel_id, day_name, open_time, close_time) 
                        values ('" . $hotelId . "', '" . $day . "', '" . $openMins . "', '" . $closeMins . "')";
                mysqli_query($connection, $sqlTiming);
            }
        }

        fclose($file);
        $dbCon->disconnect($connection);

        return true;
    }

    public function parseCSV($openingHours)
    {
        $hotelSchedule = [];
        $daysList = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

        $hotelCSVSchedule = [];
        if (strpos($openingHours, '/') !== false) {
            $hotelCSVSchedule = explode('/', $openingHours);
        } else {
            $hotelCSVSchedule[] = $openingHours;
        }

        // Regex to find the time range
        $durationRegex = "/\d+?(:\d+)? (?i)(?:[ap]m) - \d+?(:\d+)? (?i)(?:[ap]m)/";

        $durationStr = [];
        foreach ($hotelCSVSchedule as $key => $csvSchedule) {

            preg_match($durationRegex, $csvSchedule, $durationStr);

            $daysStr = substr($csvSchedule, 0, strcspn($csvSchedule, '0123456789'));
            $partsList = explode(', ', $daysStr);

            // // Split the days e.g. Mon-Thu
            $days = [];
            $days = explode('-', $partsList[0]);

            if (count($days) > 1) {
                $from = trim(preg_replace('/\s+/', '', $days[0]));
                $to = trim(preg_replace('/\s+/', '', $days[1]));
                for ($i = array_search(strip_tags($from), $daysList); $i <= array_search(strip_tags($to), $daysList); $i++) {
                    $hotelSchedule[$i] = trim($daysList[$i]) . '-' . trim($durationStr[0]);
                }
            } else {
                $hotelSchedule[array_search(trim($partsList[0]), $daysList)] = trim($partsList[0]) . '-' . trim($durationStr[0]);
            }

            if (count($partsList) == 2) {

                if (strpos($partsList[1], '-') !== false) {
                    $days = explode('-', $partsList[1]);
                    $from = trim(preg_replace('/\s+/', '', $days[0]));
                    $to = trim(preg_replace('/\s+/', '', $days[1]));
                    for ($i = array_search(strip_tags($from), $daysList); $i <= array_search(strip_tags($to), $daysList); $i++) {
                        $hotelSchedule[$i] = trim($daysList[$i]) . '-' . trim($durationStr[0]);
                    }
                } else {
                    $str = trim(preg_replace('/\s+/', '', $partsList[1]));
                    $key = array_search(strip_tags($str), $daysList);
                    $hotelSchedule[$key] = trim($partsList[1]) . '-' . trim($durationStr[0]);
                }
            }
        }

        return $hotelSchedule;
    }

    public function minutes($timeFrom, $timeTo)
    {
        $timeFrom = date("H:i", strtotime($timeFrom));
        $timeTo = date("H:i", strtotime($timeTo));

        if ($timeTo < $timeFrom) {
            $time = explode(':', $timeTo);
            $time[0] += 24;
            $minsTo = ($time[0] * 60) + ($time[1]);

            $time = explode(':', $timeFrom);
            $minsFrom = ($time[0] * 60) + ($time[1]);

            return $minsFrom . '-' . $minsTo;
        }

        $time = explode(':', $timeTo);
        $minsTo = ($time[0] * 60) + ($time[1]);

        $time = explode(':', $timeFrom);
        $minsFrom = ($time[0] * 60) + ($time[1]);

        return $minsFrom . '-' . $minsTo;
    }
}
