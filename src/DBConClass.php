<?php

namespace TDS;

class DBConClass
{
    public function connect()
    {
        $upOne = dirname(__DIR__, 1);
        include_once $upOne . '/config.php';

        return mysqli_connect($host, $user, $pass, $db);
    }

    public function disconnect($conn)
    {
        mysqli_close($conn);
    }
}
