<?php

namespace TDS;

use TDS\DBConClass;

class ImportDatabase
{
    function importDatabase()
    {
        $dbCon = new DBConClass();
        $conn = $dbCon->connect();

        $query = '';
        $directory = dirname(__DIR__, 1);
        $sqlScript = file($directory . '/uploads/db_script.sql');

        // import database
        foreach ($sqlScript as $line) {

            $startWith = substr(trim($line), 0, 2);
            $endWith = substr(trim($line), -1, 1);

            if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
                continue;
            }

            $query = $query . $line;
            if ($endWith == ';') {
                mysqli_query($conn, $query) or die('<div>Problem in executing the SQL query <b>' . $query . '</b></div>');
                $query = '';
            }
        }

        $dbCon->disconnect($conn);

        return true;
    }
}
