<?php

namespace TDS\contracts;

interface HotelClassContract
{
    public function index($day, $mins);
    public function fetchRecords($day, $mins);
}
