<?php

require_once __DIR__ . '/vendor/autoload.php';

use TDS\ImportData;
use TDS\ImportDatabase;

$type = $_REQUEST['type'];

switch ($type) {
    case 'db':
        $importDatabase = new ImportDatabase();
        $importDatabase->importDatabase();
        echo true;
        break;
    case 'data':
        $importData = new ImportData();
        $importData->seedRecords();
        echo true;
        break;
    default:
        return false;
        break;
}
